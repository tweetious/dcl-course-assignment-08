import { FlyAround, birds, LerpData } from "./modules/random-flying";
import { Timer, MoveHead } from "./modules/move-head";


import {spawnGltfX, spawnEntity, spawnBoxX, spawnPlaneX} from './modules/SpawnerFunctions'
import {BuilderHUD} from './modules/BuilderHUD'







const scene = new Entity()
const transform = new Transform({
  position: new Vector3(0, 0, 0),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
scene.addComponentOrReplace(transform)
engine.addEntity(scene)

const floorBaseGrass_01 = new Entity()
floorBaseGrass_01.setParent(scene)
const gltfShape = new GLTFShape('models/FloorBaseGrass_01/FloorBaseGrass_01.glb')
floorBaseGrass_01.addComponentOrReplace(gltfShape)
const transform_2 = new Transform({
  position: new Vector3(8, 0, 8),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(1, 1, 1)
})
floorBaseGrass_01.addComponentOrReplace(transform_2)
engine.addEntity(floorBaseGrass_01)

const building = new Entity()
building.setParent(scene)
const gltfShape_2 = new GLTFShape('models/model1/modelColliders4.glb')
building.addComponentOrReplace(gltfShape_2)
const transform_3 = new Transform({
  position: new Vector3(8, 0, 4),
  rotation: new Quaternion(0, 0, 0, 1),
  scale: new Vector3(0.9,0.9, 0.9)
})
building.addComponentOrReplace(transform_3)
engine.addEntity(building)


/*const girafee = new Entity()
girafee.setParent(scene)
const gltfShape_3 = new GLTFShape('models/giraffe/giraffe2.glb')
girafee.addComponentOrReplace(gltfShape_3)
const transform_4 = new Transform({
  //position: new Vector3(10.5, 0, 14.5),
  //rotation: new Quaternion(0, 0, 0, 1),
  position: new Vector3(3.5,0,14.5),
  rotation: new Quaternion.Euler(0, -90, 0),
  scale: new Vector3(0.03, 0.03, 0.03)
})
girafee.addComponentOrReplace(transform_4)
engine.addEntity(girafee)*/

let girafeeShape = new GLTFShape('models/giraffe/giraffe2Colliders.glb')
let girafee = spawnGltfX(girafeeShape, 3.5,0,10.5,    0, -90, 0,        0.03,0.03,0.03)


///////// Example of adding the BuilderHUD to your scene and attaching it to one existing scene entity.
//const hud:BuilderHUD =  new BuilderHUD()
//hud.attachToEntity(girafee)
//hud.attachToEntity(existingEntity2)
//hud.setDefaultParent(scene)

// Quaternion.Euler(0,0,0)

/*0,0:  --------------- BuilderHUD entities -------------
preview.js:8 0,0:  Existing(3.5,0,14.5,  0,-90,0,  0.03,0.03,0.03)
preview.js:8 0,0:  -------------------------------------------------*/

// add systems to the engine

engine.addSystem(new FlyAround())

engine.addSystem(new MoveHead())

////////////////////
// Lay out environment

const tree = new Entity()
tree.addComponent(new Transform({
  position: new Vector3(10.5, 0, 14),
  scale: new Vector3(1.4, 1.4, 1.4)
}))
tree.addComponent(new GLTFShape("models/Tree.gltf"))
tree.addComponent(new Animator)
let treeClip = new AnimationState('Tree_Action')
treeClip.looping = false
tree.getComponent(Animator).addClip(treeClip)
tree.addComponent(
  new OnClick(e => {
    let anim = tree.getComponent(Animator).getClip('Tree_Action')
    anim.stop()
    anim.play()
    log("new bird")
    newBird()
  })
)
engine.addEntity(tree)

/*const ground = new Entity()
ground.addComponent(new Transform({
  position: new Vector3(8, 0, 8),
  scale: new Vector3(1.6, 1.6, 1.6)
}))
ground.addComponent(new GLTFShape("models/Ground.gltf"))
engine.addEntity(ground)*/

/////////////////////
// Other functions

// Starting coordinates for all birds

const startPosition = new Vector3(13, 3.5, 5)
const birdScale = new Vector3(0.2, 0.2, 0.2)

// Create a new bird

let birdShape = new GLTFShape("models/hummingbird.glb")

function newBird(){
  if (birds.entities.length > 10) {return}
    const bird = new Entity()

    bird.addComponent(new Transform({
      position: startPosition,
      scale: birdScale
    }))

    bird.addComponent(birdShape)
    let birdAnim = new Animator()
    bird.addComponent(birdAnim)
    const flyAnim = new AnimationState('fly')
    flyAnim.speed = 2
    // const lookAnim = new AnimationState('look')
    // lookAnim.looping = false
    // const shakeAnim = new AnimationState('shake')
    // shakeAnim.looping = false
    birdAnim.addClip(flyAnim)
    // birdAnim.addClip(lookAnim)
    // birdAnim.addClip(shakeAnim)
    flyAnim.play()
   
    const nextPos = new Vector3((Math.random() * 12) + 2 ,(Math.random() * 3) + 1 ,(Math.random() * 12) + 2)
    bird.addComponent(new LerpData(startPosition, nextPos, 0, 200))
    bird.addComponent(new Timer())

    bird.getComponent(Transform).lookAt(nextPos)
    
    engine.addEntity(bird)
}


// Create entity
//const cube = new Entity()

// Create AudioClip object, holding audio file
const clip = new AudioClip('sounds/birds.mp3')

// Create AudioSource component, referencing `clip`
const source = new AudioSource(clip)

// Add AudioSource component to entity
tree.addComponent(source)

// Play sound
source.playing = true
source.loop = true
source.playOnce()
source.volume = 0.5