Assignment 8

Added colliders to the objects in my scene:
- added a cube collider to the giraffe, so someone could not walk through it.
- added several colliders to the perimeter of the house. I want to allow a visitor to go through the door, and have access to the internal of the house. However, I restricted the ability to walk through the walls (both from the outside & also from the inside of the house).

Added animation to my scene:
I used segments of code from the decentraland scene examples and added a tree (by adjusting them to my code scene). hummingbirds spawn when you click the tree

Added sound to my scene:

There is a sound of birds coming from the tree. You cannot see the birds (since they are hidden in the tree). You have to click on the tree in order to see them.