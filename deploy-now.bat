echo ON
rmdir /S /Q export
copy scene-export.json scene.json /Y
call npm i
call dcl build
call dcl export
copy now.json export\.
cd export
call now
cd ..
copy scene-deploy.json scene.json /Y
